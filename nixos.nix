{ lib, inputs, ... }:
{
  flake = {
    nixosModules = {
      all = {
        imports = lib.filesystem.listFilesRecursive ./modules ++ [
          inputs.nixos-generators.nixosModules.all-formats
        ];
      };
    };

    inherit (inputs.self.legacyPackages.x86_64-linux) proxmoxImages;
    inherit (inputs.self.legacyPackages.aarch64-linux) sdImages;

    # switch to configurations manually using the flake:
    #  * local machine: nixos-rebuild switch --flake .#nixbus
    #  * remote: nixos-rebuild switch --flake .#nixbus --target-host 192.168.1.1
    nixosConfigurations =
      let
        # evaluate with colemena on a fixed system since this has to be pure :/
        system = "x86_64-linux";
      in
      lib.genAttrs
        ((lib.attrNames inputs.self.legacyPackages.${system}.colmenaNixosConfigurations) ++ [ "default" ])
        (
          name:
          if name == "default" then
            lib.nixosSystem {
              pkgs = inputs.nixpkgs.legacyPackages.${system};
              modules =
                [ inputs.self.nixosModules.all ]
                ++ [
                  (
                    { config, ... }:
                    {
                      nixpkgs = {
                        inherit system;
                      };
                      system.stateVersion = config.system.nixos.release;
                    }
                  )
                ];
              specialArgs = {
                inherit name inputs;
                nodes = inputs.self.outputs.nixosConfigurations;
              };
            }
          else
            inputs.self.legacyPackages.${system}.colmenaNixosConfigurations.${name}
        );
  };

  perSystem =
    {
      pkgs,
      system,
      self',
      ...
    }:
    {
      legacyPackages =
        {
          nixosConfigurations = lib.mapAttrs' (
            host: _:
            lib.nameValuePair "test-${host}" (
              lib.nixosSystem {
                inherit pkgs;
                modules = [
                  inputs.self.nixosModules.all
                  ./hosts/${host}.nix
                  (
                    { config, ... }:
                    {
                      nixpkgs = {
                        inherit system;
                      };
                      system.stateVersion = config.system.nixos.release;
                    }
                  )
                ];
                specialArgs = {
                  inherit inputs;
                  name = "muccc";
                };
              }
            )
          ) (inputs.self.legacyPackages.${system}.colmenaNixosConfigurations // { test-default = null; });
        }
        // (lib.optionalAttrs (system == "aarch64-linux") {
          sdImages = lib.mapAttrs (
            _name: node: node.config.formats.sd-aarch64
          ) self'.legacyPackages.colmenaNixosConfigurations;
        })
        // (lib.optionalAttrs (system == "x86_64-linux") {
          # import into proxmox: https://nixos.wiki/wiki/Proxmox_Virtual_Environment#Deploying_on_proxmox
          proxmoxImages =
            {
              base = inputs.nixos-generators.nixosGenerate {
                inherit pkgs;
                format = "proxmox";
                modules = [
                  inputs.self.nixosModules.all
                  ./profiles/proxmox-image.nix
                  (
                    { config, ... }:
                    {
                      system.stateVersion = config.system.nixos.release;
                    }
                  )
                ];
                specialArgs = {
                  name = "muccc-base";
                  inherit inputs;
                };
              };
            }
            // lib.mapAttrs (
              _name: node:
              let
                image = node.extendModules { modules = [ ./profiles/proxmox-image.nix ]; };
              in
              image.config.formats.proxmox
            ) self'.legacyPackages.colmenaNixosConfigurations;

          proxmoxLxc.base = inputs.nixos-generators.nixosGenerate {
            inherit pkgs;
            format = "proxmox-lxc";
            modules = [
              inputs.self.nixosModules.all
              (
                { config, ... }:
                {
                  # HACK: remove after 24.11
                  boot.postBootCommands = ''
                    # After booting, register the contents of the Nix store in the Nix
                    # database.
                    if [ -f /nix-path-registration ]; then
                      ${config.nix.package.out}/bin/nix-store --load-db < /nix-path-registration &&
                      rm /nix-path-registration
                    fi

                    # nixos-rebuild also requires a "system" profile
                    ${config.nix.package.out}/bin/nix-env -p /nix/var/nix/profiles/system --set /run/current-system
                  '';
                  systemd.services."getty@tty1".enable = lib.mkForce true;
                  systemd.services."autovt@".enable = lib.mkForce true;
                  systemd.services."autovt@".unitConfig.ConditionPathExists = [
                    ""
                    "/dev/%I"
                  ];

                  # Hostname is being reset by proxmox-lxc module. If we set domain
                  # here we will not get a FQDN using that domain.
                  networking.domain = null;
                  system.stateVersion = config.system.nixos.release;
                }
              )
            ];
            specialArgs = {
              name = "muccc-base";
              inherit inputs;
            };
          };

          # // (pkgs.lib.traceVal (
          #   builtins.listToAttrs (
          #     builtins.concatLists (
          #       builtins.map
          #         (
          #           format:
          #           let
          #             lib = pkgs.lib;
          #             bin = "run-${format}";
          #             vmScript =
          #               bin:
          #               pkgs.writers.writeBashBin bin ''
          #                 VM_NAME=''${1:-default}
          #                 exec $(nix build --no-link --print-out-paths ".#$packages.{format}.test-$VM_NAME")
          #               '';
          #           in
          #           (lib.mapAttrs' (
          #             name: v:
          #             (lib.nameValuePair "${format}-test-${name}" (
          #               (lib.nixosSystem {
          #                 inherit pkgs;
          #                 modules = allModules ++ [
          #                   ./hosts/${name}.nix
          #                   { nixpkgs.system = lib.mkForce system; }
          #                 ];
          #                 specialArgs = {
          #                   inherit name inputs;
          #                   nodes = nixosConfigurations;
          #                 };
          #               }).config.formats.${format}
          #             ))
          #           ) nixosConfigurations)
          #           // (lib.nameValuePair bin (vmScript bin))
          #         )
          #         [
          #           "vm"
          #           "vm-nogui"
          #         ]
          #     )
          #   )
          # ))
        });
    };
}
