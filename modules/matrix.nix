{
  config,
  lib,
  pkgs,
  ...
}:
let
  cfg = config.muccc.matrix;
in
{
  options = {
    muccc.matrix = {
      enable = lib.mkEnableOption "muc.ccc.de homeserver";
      hostfqdn = lib.mkOption {
        type = lib.types.str;
        default = "matrix.muc.ccc.de";
      };
    };
  };

  config = lib.mkIf cfg.enable {
    sops.secrets = {
      matrix_synapse_registration_shared_secret = {
        owner = "matrix-synapse";
      };
      matrix_synapse_turn_shared_secret_config = {
        owner = "matrix-synapse";
      };
      matrix_synapse_oidc_providers_config = {
        owner = "matrix-synapse";
      };
      mjolnir_password = {
        owner = "mjolnir";
      };
    };

    nixpkgs.config.permittedInsecurePackages = [
      # for mjolnir
      "olm-3.2.16"
    ];

    networking.firewall.allowedTCPPorts = [
      80
      443
      8448
    ];

    services.postgresql = {
      enable = true;
      ensureDatabases = [ "matrix-synapse" ];
      ensureUsers = [
        {
          name = "matrix-synapse";
          ensureDBOwnership = true;
        }
      ];
    };

    services.nginx = {
      enable = true;
      virtualHosts = {
        ${cfg.hostfqdn} = {
          forceSSL = true;
          enableACME = true;
          locations = {
            "/" = {
              extraConfig = ''
                add_header X-Content-Type-Options "nosniff" always;
                add_header X-Frame-Options "SAMEORIGIN" always;
                add_header Strict-Transport-Security "max-age=31536000; includeSubdomains" always;
              '';
              root = pkgs.element-web.override {
                conf = {
                  default_server_config = {
                    "m.homeserver" = {
                      base_url = "https://matrix.muc.ccc.de";
                      server_name = "muc.ccc.de";
                    };
                  };
                  permalink_prefix = "https://matrix.muc.ccc.de";
                  branding = {
                    auth_header_logo_url = "https://muc.ccc.de/_media/wiki:logo.png";
                    auth_footer_links = [
                      {
                        text = "muc.ccc.de";
                        url = "https://muc.ccc.de";
                      }
                    ];
                  };
                  disable_custom_urls = true;
                  disable_guests = true;
                  disable_3pid_login = true;
                  brand = "muccc matrix";
                  show_labs_settings = true;
                  default_federate = true;
                  default_theme = "dark";
                  default_country_code = "DE";
                  room_directory = {
                    servers = [
                      "muc.ccc.de"
                      "hackint.org"
                    ];
                  };
                  settings_defaults = {
                    "MessageComposerInput.showStickersButton" = false;
                    "UIFeature.shareSocial" = false;
                    "UIFeature.locationSharing" = false;
                    "UIFeature.widgets" = false;
                    "UIFeature.identityServer" = false;
                    "UIFeature.thirdPartyId" = false;
                    "UIFeature.registration" = false;
                    "UIFeature.passwordReset" = false;
                    "urlPreviewsEnabled" = false;
                  };
                  enable_presence_by_hs_url = {
                    "https://matrix.org" = false;
                    "https://matrix-client.matrix.org" = false;
                  };
                  integrations_ui_url = null;
                  integrations_rest_url = null;
                  integrations_widgets_urls = null;
                  jitsi.preferred_domain = "webex.muc.ccc.de";
                };
              };
            };
            "~ ^(/_matrix|/_synapse/client)" = {
              proxyPass = "http://localhost:8008";
              extraConfig = ''
                client_max_body_size 50m;
                proxy_http_version 1.1;
              '';
            };
            "/.well-known/matrix/server" = {
              extraConfig = ''
                default_type application/json;
                return 200 '{"m.server": "${cfg.hostfqdn}"}';
              '';
            };
            "/.well-known/matrix/client" = {
              extraConfig = ''
                default_type application/json;
                return 200 '{ "m.homeserver": { "base_url": "https://${cfg.hostfqdn}" }, "im.vector.riot.jitsi": { "preferredDomain": "webex.muc.ccc.de" } }';
                add_header Access-Control-Allow-Origin *;
              '';
            };
          };
        };
      };
    };

    users.extraGroups.matrix-synapse.members = [ "nginx" ];

    security.acme.certs = {
      ${cfg.hostfqdn} = {
        group = "matrix-synapse";
        postRun = ''
          systemctl restart matrix-synapse
        '';
      };
    };

    services.matrix-synapse = {
      enable = true;
      withJemalloc = true;
      extras = [
        "systemd"
        "oidc"
        "postgres"
        "url-preview"
        "user-search"
      ];
      settings = {
        server_name = "muc.ccc.de";
        public_baseurl = "https://${cfg.hostfqdn}";
        database_type = "psycopg2";
        enable_registration = false;
        registration_requires_token = true;
        registration_shared_secret_path =
          config.sops.secrets.matrix_synapse_registration_shared_secret.path;
        enable_metrics = true;
        tls_certificate_path = "/var/lib/acme/${cfg.hostfqdn}/fullchain.pem";
        tls_private_key_path = "/var/lib/acme/${cfg.hostfqdn}/key.pem";
        listeners = lib.mkForce [
          {
            port = 8008;
            bind_addresses = [
              "127.0.0.1"
              "::1"
            ];
            type = "http";
            tls = false;
            x_forwarded = true;
            resources = [
              {
                names = [
                  "client"
                  "federation"
                ];
                compress = false;
              }
            ];
          }
          {
            port = 8448;
            bind_addresses = [ "::" ];
            type = "http";
            tls = true;
            resources = [
              {
                names = [
                  "federation"
                  "client"
                ];
              }
            ];
          }
        ];
        allow_public_rooms_without_auth = true;
        allow_public_rooms_over_federation = true;
        admin_contact = "mailto:noc@muc.ccc.de";
        federation_client_minimum_tls_version = "1.2";
        presence.enabled = false;
        user_ips_max_age = "7d";
        forgotten_room_retention_period = "14d";
        media_retention.remote_media_lifetime = "30d";
        limit_remote_rooms = {
          enabled = true;
          complexity = 5;
          complexity_error = "Room too big to join. Contact infra team if you think this is an error.";
          admins_can_join = false;
        };
        rc_login = {
          address = {
            per_second = 0.1;
            burst_count = 5;
          };
          account = {
            per_second = 0.1;
            burst_count = 5;
          };
          failed_attempts = {
            per_second = 0.1;
            burst_count = 3;
          };
        };
        turn_uris = [
          "turn:webex.muc.ccc.de?transport=udp"
          "turn:webex.muc.ccc.de?transport=tcp"
        ];
        turn_user_lifetime = "5h";
      };
      extraConfigFiles = [
        config.sops.secrets.matrix_synapse_turn_shared_secret_config.path
        config.sops.secrets.matrix_synapse_oidc_providers_config.path
      ];
    };

    systemd.timers.matrix-synapse-compress-state = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "5:30";
        Persistent = true;
      };
    };

    systemd.services.matrix-synapse-compress-state = {
      requires = [ "postgresql.service" ];
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${pkgs.matrix-synapse-tools.rust-synapse-compress-state}/bin/synapse_auto_compressor -p 'postgresql:///matrix-synapse?host=/run/postgresql&user=matrix-synapse' -c 500 -n 100";
        User = "matrix-synapse";
        Group = "matrix-synapse";
      };
    };

    services.mjolnir = {
      enable = true;
      homeserverUrl = "https://${cfg.hostfqdn}";
      pantalaimon = {
        enable = true;
        username = "mjolnir";
        passwordFile = config.sops.secrets.mjolnir_password.path;
      };
      protectedRooms = [
        "https://matrix.to/#/!uciUIKjQUAmUEKasZk:muc.ccc.de" # bot-test
      ];
      managementRoom = "!YGxWBifruuvsJxtBro:muc.ccc.de";
      settings = {
        autojoinOnlyIfManager = true;
        recordIgnoredInvites = true;
        automaticallyRedactForReasons = [
          "spam"
          "advertising"
        ];
        commands = {
          additionalPrefixes = [ "mod" ];
        };
      };
    };
  };
}
