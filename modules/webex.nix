{ lib, config, ... }:

let
  inherit (builtins) toString;
  cfg = config.muccc.webex;
in
{
  options.muccc.webex = {
    enable = lib.mkEnableOption "WebEx Jitsi";
  };

  config = lib.mkIf cfg.enable {
    networking = {
      firewall.allowedTCPPorts = [
        config.services.coturn.listening-port
        config.services.coturn.tls-listening-port
        config.services.coturn.alt-listening-port
        config.services.coturn.alt-tls-listening-port
      ];
      firewall.allowedUDPPorts = [
        config.services.coturn.listening-port
        config.services.coturn.tls-listening-port
        config.services.coturn.alt-listening-port
        config.services.coturn.alt-tls-listening-port
      ];
      firewall.allowedUDPPortRanges = [
        {
          from = config.services.coturn.min-port;
          to = config.services.coturn.max-port;
        }
      ];
    };

    # FIXME
    nixpkgs.config.permittedInsecurePackages = [ "jitsi-meet-1.0.8043" ];

    services.nginx.virtualHosts."webex.muc.ccc.de".extraConfig = ''
      more_set_headers "Content-Security-Policy: frame-ancestors https://* vector://vector";
    '';

    services.jitsi-meet = {
      enable = true;
      hostName = "webex.muc.ccc.de";
      nginx.enable = true;

      config = {
        startWithVideoMuted = true;
        desktopSharingFrameRate = {
          min = 5;
          max = 33;
        };
        channelLastN = 11;
        prejoinConfig = {
          enabled = true;
        };
        enableWebHIDFeature = true;
        disableThirdPartyRequests = true;
        analytics = {
          enabled = false;
        };
        liveStreamingEnabled = false;
        fileRecordingsEnabled = false;
        useStunTurn = true;
        p2p = {
          useStunTurn = true;
          stunServers = [
            { urls = "stun:${config.services.coturn.realm}:${toString config.services.coturn.listening-port}"; }
          ];
        };
      };

      interfaceConfig = {
        SHOW_JITSI_WATERMARK = false;
      };
    };

    services.jitsi-videobridge = {
      openFirewall = true;
    };

    services.prosody = {
      # FIXME: TURN currently doesn't work because prosody does not provide any TURN servers
      extraModules = [ "turncredentials" ];
      extraConfig = ''
        turncredentials_secret = os.getenv("PROSODY_TURN_SECRET")
        turncredentials_host = "${config.services.coturn.realm}"

        log = { warn = "*syslog"; }
      '';
    };

    systemd.services.prosody.serviceConfig.EnvironmentFile = [
      config.sops.secrets.prosody_turn_secret.path
    ];
    systemd.services.jicofo = {
      after = [ "prosody.service" ];
      environment.JICOFO_MAX_MEMORY = "1024m";
    };
    systemd.services.jitsi-videobridge2.environment.VIDEOBRIDGE_MAX_MEMORY = "1024m";

    services.coturn = {
      enable = true;
      cert = "/var/lib/acme/webex.muc.ccc.de/fullchain.pem";
      pkey = "/var/lib/acme/webex.muc.ccc.de/key.pem";
      realm = config.services.jitsi-meet.hostName;
      use-auth-secret = true;
      static-auth-secret-file = config.sops.secrets.coturn_auth_secret.path;
    };

    users.extraGroups.nginx.members = [ "turnserver" ];
  };
}
