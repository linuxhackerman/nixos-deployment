{
  modulesPath,
  lib,
  config,
  pkgs,
  ...
}:
{
  system.stateVersion = "24.11";

  services.postgresql.package = pkgs.postgresql_17;

  sops = {
    defaultSopsFile = ../secrets/stammstrecke.yaml;
    secrets = {
      wireguard_private_key = {
        owner = "systemd-network";
      };
      wireguard_psk = {
        owner = "systemd-network";
      };
    };
  };

  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
  ];

  boot.initrd.availableKernelModules = [
    "virtio_pci"
    "virtio_scsi"
    "ahci"
    "sd_mod"
  ];
  boot.kernelParams = [ "console=ttyS0,19299n8" ];
  boot.loader.grub.extraConfig = ''
    serial --speed-19200 --unit=0 --word=8 --parity=no --stop=1;
    terminal_input serial;
    terminal_output serial
  '';
  boot.loader.grub.forceInstall = true;
  boot.loader.grub.device = lib.mkForce "nodev";
  boot.loader.timeout = 10;

  fileSystems."/" = {
    device = "/dev/sda";
    fsType = "ext4";
  };

  swapDevices = [
    { device = "/dev/sdb"; }
  ];

  networking = {
    firewall = {
      allowedUDPPorts = [
        23425
        15777
        15000
        7777
        27015
      ];
      allowedUDPPortRanges = [
        {
          from = 27031;
          to = 27036;
        }
      ];
      allowedTCPPorts = [
        7777
        27015
        27036
      ];
    };
    nat = {
      enable = true;
      externalInterface = "upl0nk";
      internalInterfaces = [ "muccc" ];
      forwardPorts =
        # satisfactory on zock
        lib.map (e: e // { destination = "83.133.178.79"; }) [
          {
            proto = "tcp";
            sourcePort = 7777;
          }
          {
            proto = "tcp";
            sourcePort = 27015;
          }
          {
            proto = "tcp";
            sourcePort = 27036;
          }
          {
            proto = "udp";
            sourcePort = 7777;
          }
          {
            proto = "udp";
            sourcePort = 15777;
          }
          {
            proto = "udp";
            sourcePort = 27015;
          }
          {
            proto = "udp";
            sourcePort = 30000;
          }
          {
            proto = "udp";
            sourcePort = 27031;
          }
          {
            proto = "udp";
            sourcePort = 27032;
          }
          {
            proto = "udp";
            sourcePort = 27033;
          }
          {
            proto = "udp";
            sourcePort = 27034;
          }
          {
            proto = "udp";
            sourcePort = 27035;
          }
          {
            proto = "udp";
            sourcePort = 27036;
          }
        ];
    };
  };

  systemd.network.links."30-upl0nk" = {
    matchConfig.PermanentMACAddress = "f2:3c:95:8d:46:b5";
    linkConfig.Name = "upl0nk";
  };
  systemd.network.networks."30-upl0nk" = {
    matchConfig.Name = "upl0nk";
    networkConfig = {
      Address = [
        "45.79.249.152/24"
        "2a01:7e01::f03c:95ff:fe8d:46b5/128"
      ];
      Gateway = [
        "45.79.249.1"
        "fe80::1"
      ];
      DNS = [
        "2a01:7e01::5"
        "2a01:7e01::c"
        "139.162.134.5"
        "139.162.133.5"
      ];
    };
    routes = [
      {
        # weird linode routing
        Destination = "172.105.79.0/24";
      }
    ];
  };

  boot.kernel.sysctl = {
    "net.ipv4.ip_forward" = 1;
    "net.ipv6.conf.all.forwarding" = 1;
  };

  systemd.network.netdevs."10-muccc" = {
    netdevConfig = {
      Name = "muccc";
      Kind = "wireguard";
      MTUBytes = "1420";
    };
    wireguardConfig = {
      PrivateKeyFile = config.sops.secrets.wireguard_private_key.path;
      ListenPort = 23425;
      RouteTable = "main";
    };
    wireguardPeers = [
      # garching
      {
        PublicKey = "pqfhiGTTaW+kniwL7bYQBTfciJ3BNkN21euvpedl8Fg=";
        PresharedKeyFile = config.sops.secrets.wireguard_psk.path;
        AllowedIPs = [
          "100.123.42.2/32"
          "2a01:7e01:e003:8b00:ffff::2/128"
          # routed
          "172.105.79.17/32"
          "2a01:7e01:e003:8b00::/56"
          # environment
          "2001:7f0:3003:beef::/64"
          "83.133.178.64/26"
        ];
        PersistentKeepalive = 60;
      }
    ];
  };
  systemd.network.networks."10-muccc" = {
    matchConfig.Name = "muccc";
    address = [
      "2a01:7e01:e003:8b00:ffff::1/128"
      "100.123.42.3/32"
    ];
    networkConfig = {
      LLMNR = false;
      MulticastDNS = false;
    };
  };

  muccc.matrix.enable = true;

  services.longview = {
    enable = true;
    apiKeyFile = "/var/lib/longview/apiKeyFile";
  };
}
