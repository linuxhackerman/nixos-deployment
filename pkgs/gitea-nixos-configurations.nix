{ lib, colmenaNixosConfigurations }:

let
  hosts = [ "nginx" ];
  nixosConfigurations = lib.filterAttrs (
    name: _: builtins.elem name hosts
  ) colmenaNixosConfigurations;
in
lib.attrsets.mapAttrs (_: v: v.config.system.build.toplevel) nixosConfigurations
